# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
#~
import requests
import datetime
from openerp.http import request
from openerp.addons.web import http
from openerp import http
from openerp import models, fields, api, _
from openerp.exceptions import Warning
from datetime import datetime
from openerp.tools.misc import DEFAULT_SERVER_DATETIME_FORMAT as DTF
import re

class hr_leave_permission(models.Model):
    _name = "hr.leave.permission"

    _inherit = ['mail.thread', 'ir.needaction_mixin']

    employee_id = fields.Many2one('hr.employee', 'Employee')
    reason = fields.Char('Reason', required=True)
    st_date = fields.Date('Date', required=True)
    hour  = fields.Float('Time')

class hr_attendance(models.Model):
    _inherit = "hr.attendance"
    
    company_id = fields.Many2one('res.company', 'Company')
    
    @api.onchange('employee_id')
    def onchange_employee_id(self):
        if self.employee_id:
            self.company_id = self.employee_id.company_id.id
            
    
    def _altern_si_so(self, cr, uid, ids, context=None):
        return True
