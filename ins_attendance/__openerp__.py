# -*- coding: utf-8 -*-
{
    'name': " Internal Attendance",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "Insoft",
    'website': "http://www.insoft.com",
    'category': 'Hr',
    'version': '1.0',
    'depends': ['base', 'hr', 'hr_holidays'],
    'data': [
        'templates.xml',
        'hr_attendance.xml',
        'leave_permission.xml',
        'security/ir.model.access.csv',
    ],
}
