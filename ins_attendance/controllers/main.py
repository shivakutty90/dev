# -*- coding: utf-8 -*-
import werkzeug

from openerp import http, fields
from openerp.http import request
from openerp import SUPERUSER_ID
from openerp.tools.translate import _


class Attendance(http.Controller):
    @http.route('/attendance', type='http', auth="public")
    def ins_attendance(self, inputempid=None, **kw):
        cr, uid, context, registry = request.cr, request.uid, request.context, request.registry
        request.cr.execute(
            '''SELECT he.identification_id,
                   he.name_related
            FROM   hr_employee he,
                   resource_resource rr
            WHERE  he.id IN (SELECT he.id
                             FROM   hr_employee he
                             EXCEPT
                             SELECT ha.employee_id
                             FROM   hr_attendance ha
                             WHERE  ha.name :: text LIKE CURRENT_DATE|| '%')
                   AND rr.id = he.resource_id
                   AND rr.active = true
            ORDER BY rr.company_id, name_related'''
        )
        employees = map(list, request.cr.fetchall())

        request.cr.execute(
            """SELECT he.identification_id,
                   he.name_related,
                   Split_part (Split_part (( ha.name AT TIME zone 'UTC' ) :: text, ' ', 2),
                   '+', 1)
            FROM   hr_attendance ha,
                   hr_employee he,
                   resource_resource rr
            WHERE  ha.id IN (SELECT Max(id) AS id
                             FROM   hr_attendance
                             WHERE  name :: text LIKE current_date
                                                      ||'%'
                             GROUP  BY employee_id)
                   AND he.id = ha.employee_id
                   AND action = 'sign_in'
                   AND rr.id = he.resource_id
            ORDER  BY ha.id DESC,
                      he.name_related"""
        )
        signed_in_emp = map(list, request.cr.fetchall())

        request.cr.execute(
            """SELECT he.identification_id,
                   he.name_related,
                   har.name,
                   Split_part (Split_part (( ha.name AT TIME zone 'UTC' ) :: text, ' ', 2),
                   '+', 1)
            FROM   hr_attendance ha,
                   hr_employee he,
                   hr_action_reason har,
                   resource_resource rr
            WHERE  ha.id IN (SELECT Max(id) AS id
                             FROM   hr_attendance
                             WHERE  name :: text LIKE current_date
                                                      ||'%'
                             GROUP  BY employee_id)
                   AND he.id = ha.employee_id
                   AND action = 'sign_out'
                   AND har.id = ha.action_desc
                   AND rr.id = he.resource_id
            ORDER  BY ha.id DESC,
                      rr.company_id,
                      he.name_related"""
        )
        signed_out_emp = map(list, request.cr.fetchall())

        values = {
            'employees': employees,
            'signed_in_emp': signed_in_emp,
            'signed_out_emp': signed_out_emp,
        }
        if inputempid:
            identification_id = inputempid
            employee_obj = registry.get('hr.employee')
            employee_sr = employee_obj.search(cr, 1, [('identification_id', '=', identification_id.strip())])
            employee = employee_obj.browse(cr, uid, employee_sr)
            if not employee:
                values['error'] = _("Could not Find a Employee Id.")
            else:
                today = fields.Date.today() + '%'
                request.cr.execute(
                    "SELECT action FROM hr_attendance WHERE name::TEXT LIKE %s AND employee_id = %s ORDER BY id DESC",
                    (today, employee.id,))

                action = request.cr.fetchone()

                if action is None:
                    action = 'sign_in'
                elif action and action[0] == 'sign_in':
                    action = 'sign_out'
                elif action and action[0] == 'sign_out':
                    action = 'sign_in'

                hr_reasons = request.env['hr.action.reason'].search([('action_type', '=', 'sign_out')])
                return request.render("ins_attendance.ins_attendance_modal",
                              {'employee': employee, 'action': action, 'hr_reasons': hr_reasons})
        return request.render("ins_attendance.ins_attendance", values)


    @http.route('/attendance/action', type='http', auth="public")
    def ins_attendance_action(self, employee_id=None, action=None, action_desc=None, **post):
        cr, uid, context, registry = request.cr, request.uid, request.context, request.registry
        attendance = registry.get('hr.attendance')
        employee_obj = registry.get('hr.employee')
        action_desc = int(action_desc) if isinstance(action_desc, unicode) else None
        uid = int(1)
        employee = employee_obj.browse(cr, uid, [int(employee_id)])
        print employee_id, '^^^^^^^^^^^^^^^^^^^^^^^^', action, action_desc
        attendance.create(cr, uid, {'employee_id': employee.id, 'action': action, 'action_desc': action_desc, 'company_id': employee.company_id.id}, context=context)
        return werkzeug.utils.redirect('/attendance')
