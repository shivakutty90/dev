# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': 'Customer & Lead Management System',
    'version': '1.0',
    'depends': ['crm', 'report'],
    'author': 'Shiva',
    'description': """1.Lead Management
        \na.Lead Contact management-managed and Organise Updates, follow up, and track leads
        \nb.Leads Nurturing
        \nc.Lead Scoring
        \nd.Lead Segmentation
        \ne.Lead qualification
        \nf.Import Leads
        \ng.Deduplication
        \nh.Lead distribution- Assignation Rules
        \ni.Lead Dashboard
        \nj.Analytics on leads
        \nk.Track your sales reps’ progress in real-time
        \n
        \n
        \n2.CUSTOMERS Management
        \na.Manually/  Automatically collect and add new Customers profile into listing
        \nb.Customer’s information (e.g Name, Personal detail, Contact details, Billing address, Company information, delivery addresses...etc)
        \nc.Import existing contacts from excel
        \nd.view/retrieve a list of all content that belongs to a particular tag or filter search results based on a tag or tags
        \ne.Get a clear address book shared amongst sales persons - list of all existing and new customers names, company name, and address and contact
        \nf.Set customer preferences easily: delivery methods, financial data, etc.
        \ng.Have multiple addresses and contacts for a single company.
        \nh.Can Write notes on last conversation
        \ni.Get the full history of activities attached to any customer: opportunities, orders, invoices, total due, etc.
        \nj.Reports and Dashboards
    """,
    'website': 'http://www.odoo.com',
    'category': 'crm',
    'demo': [],
    'test': [],
    'data': [
    'crm_lead_data.xml',
    'views/crm_lead_view.xml',
    'views/report_quick_leads.xml',
    'crm_report.xml',
    
    
    ],
    'auto_install': False,
    'installable': True,
}
