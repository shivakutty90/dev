# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import time
from openerp.report import report_sxw
from openerp.osv import osv
from datetime import datetime,timedelta,date
from dateutil.relativedelta import relativedelta
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT

class quick_leads_report(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        super(quick_leads_report, self).__init__(cr, uid, name, context=context)
        ids = context.get('active_ids')
        invoice_obj = self.pool['crm.lead']
        docs = invoice_obj.browse(cr, uid, ids, context)
        self.localcontext.update({
            'docs': docs,
            'tax_grouping': self._get_tax_grouping,
        })
        self.context = context

    def _get_tax_grouping(self, docs):
        print '----------------------------------------------------------------------'
        return taxes

    

class report_quick_leads(osv.AbstractModel):
    _name = 'report.crm_custom.report_quick_leads'
    _inherit = 'report.abstract_report'
    _template = 'crm_custom.report_quick_leads'
    _wrapped_report_class = quick_leads_report

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
