# -*- coding: utf-8 -*-
##############################################################################
#
#    Odoo, Open Source Management Solution
#    Copyright (C) 2004-2015 Catsanddogs (<http://www.catsanddogs.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import fields,api,models, exceptions
from datetime import date, datetime, timedelta
from collections import defaultdict, Counter
import logging

class crm_lead(models.Model):
    _inherit = 'crm.lead'

    @api.one
    def next_button(self):
        case_stage_id = None
        lead = self.browse(self.id)
        if not lead.stage_id.name:
            case_stage_id = self.env['crm.case.stage'].search([('name', '=', 'Followed-Up'), ('type', '=', 'lead')])
            self.write({'stage_id': case_stage_id.id, 'lead_stage':case_stage_id.name})
        return True
